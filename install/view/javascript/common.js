function getURLVar(key) {
    var value = [];
    
    var query = String(document.location).split('?');
    
    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
} 

// Faster
$(document).ready(function() {
    route = getURLVar('route');

    if (!route) {
      $('#menu #home').addClass('active');
  } else {
      part = route.split('/');

      url = part[0];

      if (part[1]) {
         url += '/' + part[1];
     }		

     $('#menu a[href*=\'index.php?route=' + url + '\']').parents('li[id]').addClass('active');
 }
});


// Slower but more stable
$(window).load(function(){

    $(window).resize(function(){
        if($(body).hasClass('header-fixed')){
            if($(window).width() > 768){
                $("body").css("padding-top",$(".header").height());
            }else{
                $("body").removeAttr('style'));
            }
        }else{
            $("body:not(.common-home)").css("padding-top",$(".header").height());
        }
    }).resize();

    // Allow number in input box only
    var rex = "^-?\d+(?:\.\d+)?$";
    $(".number-only").keydown(function (e) {

        filter = [46, 8, 9, 27, 13, 110, 190];  
        if($(this).hasClass('integer') || $(this).val().indexOf('.') > -1){
            filter = [46, 8, 9, 27, 13, 190];
        }
        
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, filter) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {return;} // Retrun key press value

    });


    $("#scroll-top").click(function(e){
        e.preventDefault();
        speed = 800;
        if($(".top-scroll-helper").attr('data-speed')){
            speed = $(".top-scroll-helper").attr('data-speed');
        }
        scrollTop(speed);
    });

    if($(".follow").length){
        $(window).scroll(function(){
            if($(this).width() > 768){
                var value = $(this).scrollTop();
                $(".follow").css({'margin-top':value,'margin-bottom':-value});
            }
            else{
                $(".follow").removeAttr('style');
            }
        }).scroll();
    }
});


function debug(x){console.log(x);}
function leadingZero(x){if(x < 0){ return "0"+x;}else{return x;}}