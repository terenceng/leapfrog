<?php
class ControllerInstallStep3 extends Controller {
	private $error = array();

	public function index() { 

		if(!$this->validate_support()){
			$this->session->data['error_support'] = $this->error;
			$this->response->redirect($this->url->link('install/step_2'));
		}

		$this->language->load('install/step_3');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) { 
			$this->load->model('install/install');

			$this->model_install_install->database($this->request->post);

			// Longer but easier to upgrade
			// Define
			$def_hacks = array(
				'URL'		=>	'$_SERVER[\'HTTP_HOST\'] . dirname($_SERVER[\'PHP_SELF\']).\'/\'',
				'BASE_DIR'	=>	'str_replace("\\\","/",realpath(dirname(__FILE__))."/")',
				);

			$def_protocol = array(
				'HTTP_SERVER'	=>	'http://',
				'HTTPS_SERVER'	=>	'http://',
				);

			$def_dir = array(
				'DIR_APPLICATION'	=>	'catalog/',
				'DIR_SYSTEM'		=>	'system/',
				'DIR_IMAGE'			=>	'image/',
				'DIR_LANGUAGE'		=>	'catalog/language/',
				'DIR_TEMPLATE'		=>	'catalog/view/theme/',
				'DIR_CONFIG'		=>	'system/config/',
				'DIR_CACHE'			=>	'system/storage/cache/',
				'DIR_DOWNLOAD'		=>	'system/storage/download/',
				'DIR_LOGS'			=>	'system/storage/logs/',
				'DIR_MODIFICATION'	=>	'system/storage/modification/',
				'DIR_UPLOAD'		=>	'system/storage/upload/',
				
				// v3
				//'DIR_STORAGE'		=>	'system/storage/',
				//'DIR_CACHE'		=>	'system/cache/',
				//'DIR_SESSION'		=>	'system/session/',
				);

			$def_db = array( 
				'DB_DRIVER'		=>	addslashes($this->request->post['db_driver']),
				'DB_HOSTNAME'	=>	addslashes($this->request->post['db_hostname']),
				'DB_USERNAME'	=>	addslashes($this->request->post['db_username']),
				'DB_PASSWORD'	=>	addslashes(html_entity_decode($this->request->post['db_password'], ENT_QUOTES, 'UTF-8')),
				'DB_DATABASE'	=>	addslashes($this->request->post['db_database']),
				'DB_PORT'		=>	addslashes($this->request->post['db_port']),
				'DB_PREFIX'		=>	addslashes($this->request->post['db_prefix']),
				);
			// End Define

			// Craft
			$output = '<?php' . "\n";
			$output .= '// Hackes' . "\n";
			foreach($def_hacks as $var => $val){$output .= "define('$var', $val);\n";}

			$output .= "// Dynamic Protocol Settings\n";
			$output .= '$protocol = "http://";' . "\n"; 
			$output .= 'if($_SERVER["SERVER_PORT"] == 443){';
			$output .= '	$protocol = "https://";';
			$output .= '}';

			$output .= "\n".'// HTTP Protocol' . "\n";
			foreach($def_protocol as $var => $val){$output .= 'define(\''.$var.'\', $protocol . URL);'. "\n";}
			$output .= "\n".'// Directory' . "\n";
			foreach($def_dir as $var => $val){$output .= "define('$var', BASE_DIR. '$val');\n";}
			$output .= "\n".'// DB' . "\n";
			foreach($def_db as $var => $val){$output .= "define('$var', '$val');\n";}

			$output .= "\n";
			$output .= "define('ADVANCE_PASSWORD', false);\n";
			$output .= "define('ADMIN_FOLDER', 'admin');\n";
			
			$file = fopen(DIR_OPENCART . 'config.php', 'w');

			fwrite($file, $output);
			fclose($file);
			//End Craft

			// Admin
			$def_hacks = array(
				'URL'		=>	'$_SERVER[\'HTTP_HOST\'] . str_replace(\'/admin\', \'\', dirname($_SERVER[\'PHP_SELF\']))."/"',
				'BASE_DIR'	=>	'str_replace(DIRECTORY_SEPARATOR,"/",str_replace(DIRECTORY_SEPARATOR . "admin", "", realpath(dirname(__FILE__))))."/"',
				);
			$def_protocol = array(
				'HTTP_SERVER'	=>	'http://',
				'HTTPS_SERVER'	=>	'http://',
				);
			$def_catalog = array(
				'HTTP_CATALOG'	=>	'http://',
				'HTTPS_CATALOG'	=>	'http://',
				);

			$def_dir = array(
				'DIR_APPLICATION'	=>	'admin/',
				'DIR_SYSTEM'		=>	'system/',
				'DIR_IMAGE'			=>	'image/',
				'DIR_LANGUAGE'		=>	'admin/language/',
				'DIR_TEMPLATE'		=>	'admin/view/template/',
				'DIR_CONFIG'		=>	'system/config/',
				'DIR_CACHE'			=>	'system/storage/cache/',
				'DIR_DOWNLOAD'		=>	'system/storage/download/',
				'DIR_LOGS'			=>	'system/storage/logs/',
				'DIR_MODIFICATION'	=>	'system/storage/modification/',
				'DIR_UPLOAD'		=>	'system/storage/upload/',
				'DIR_CATALOG'		=>	'catalog/',

				// v3
				//'DIR_STORAGE'		=>	'system/storage/',
				//'DIR_CACHE'			=>	'system/cache/',
				//'DIR_SESSION'		=>	'system/session/',	
				//'OPENCART_SERVER'	=>	'https://www.opencart.com/',

				'DIR_EXCEL'			=>	'system/storage/excel/',
				'DIR_EXCEL_TPL'		=>	'system/storage/excel/tpl/',
				'DIR_EXCEL_GEN'		=>	'system/storage/excel/tpl/_generated/',
				);

			// End Define

			// Craft
			$output = '<?php' . "\n";
			$output .= '// Hackes' . "\n";
			foreach($def_hacks as $var => $val){$output .= "define('$var', $val);\n";}
			
			$output .= "// Dynamic Protocol Settings\n";
			$output .= '$protocol = "http://";' . "\n";
			$output .= 'if($_SERVER["SERVER_PORT"] == 443){';
			$output .= '	$protocol = "https://";';
			$output .= '}';

			$output .= "\n".'// HTTP Protocol' . "\n";
			foreach($def_protocol as $var => $val){$output .= 'define(\''.$var.'\', $protocol . URL. \'admin/\');' . "\n";}
			
			$output .= "\n".'// HTTP Protocol Catalog' . "\n";
			foreach($def_catalog as $var => $val){$output .= 'define(\''.$var.'\', $protocol . URL);' . "\n";}
			
			$output .= "\n".'// Directory' . "\n";
			foreach($def_dir as $var => $val){$output .= "define('$var', BASE_DIR. '$val');\n";}
			
			$output .= "\n".'// DB' . "\n";
			foreach($def_db as $var => $val){$output .= "define('$var', '$val');\n";}

			$output .= "\n";
			$output .= "define('BANNER_EXTRA', true);\n";
			$output .= "define('ADVANCE_PASSWORD', false);\n";
			$output .= "define('ADMIN_FOLDER', 'admin');\n";
		
			$file = fopen(DIR_OPENCART . 'admin/config.php', 'w');

			fwrite($file, $output);
			fclose($file);
			// End Craft


			// Update browser-sync
			$current_full_path		= str_replace(array('\\', '/'), '', getcwd());
			$current_folder_name	= basename(__DIR__);
			$document_root_path		= $_SERVER['DOCUMENT_ROOT'];

			$find = str_replace(array('\\', '/'), '', array(
				$current_folder_name,
				$document_root_path
			));

			$project_name = str_replace($find, '', $current_full_path);

			$output = '/*Browser-sync configuration*/' . "\n";
			$output .= 'module.exports = {' . "\n";
			$output .= '	port: 80,' . "\n";
			$output .= '	proxy: \'localhost/' . $project_name . '\',' . "\n";
			$output .= '	files: [' . "\n";
			$output .= '		\'catalog/view/theme/default/stylesheet/sass/*.sass\', ' . "\n";
			$output .= '		\'catalog/view/theme/default/stylesheet/sass/*.css\', ' . "\n";
			$output .= '		\'catalog/view/theme/default/stylesheet/*.sass\', ' . "\n";
			$output .= '		\'catalog/view/theme/default/stylesheet/*.css\', ' . "\n";
			$output .= '		\'catalog/view/javascript/*.js\', ' . "\n";
			$output .= '		\'catalog/view/theme/default/template/**/*.tpl\'' . "\n";
			$output .= '	]' . "\n";
			$output .= '};' . "\n";

			file_put_contents($document_root_path . '/' . $project_name . '/bs-config.js', $output);
			// End update browser-sync

			$this->response->redirect($this->url->link('install/step_4'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_step_3'] = $this->language->get('text_step_3');
		$data['text_db_connection'] = $this->language->get('text_db_connection');
		$data['text_db_administration'] = $this->language->get('text_db_administration');
		$data['text_mysqli'] = $this->language->get('text_mysqli');
		$data['text_mpdo'] = $this->language->get('text_mpdo');
		$data['text_pgsql'] = $this->language->get('text_pgsql');

		$data['entry_db_driver'] = $this->language->get('entry_db_driver');
		$data['entry_db_hostname'] = $this->language->get('entry_db_hostname');
		$data['entry_db_username'] = $this->language->get('entry_db_username');
		$data['entry_db_password'] = $this->language->get('entry_db_password');
		$data['entry_db_database'] = $this->language->get('entry_db_database');
		$data['entry_db_port'] = $this->language->get('entry_db_port');
		$data['entry_db_prefix'] = $this->language->get('entry_db_prefix');
		$data['entry_username'] = $this->language->get('entry_username');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['db_hostname'])) {
			$data['error_db_hostname'] = $this->error['db_hostname'];
		} else {
			$data['error_db_hostname'] = '';
		}

		if (isset($this->error['db_username'])) {
			$data['error_db_username'] = $this->error['db_username'];
		} else {
			$data['error_db_username'] = '';
		}

		if (isset($this->error['db_database'])) {
			$data['error_db_database'] = $this->error['db_database'];
		} else {
			$data['error_db_database'] = '';
		}
		
		if (isset($this->error['db_port'])) {
			$data['error_db_port'] = $this->error['db_port'];
		} else {
			$data['error_db_port'] = '';
		}
		
		if (isset($this->error['db_prefix'])) {
			$data['error_db_prefix'] = $this->error['db_prefix'];
		} else {
			$data['error_db_prefix'] = '';
		}

		if (isset($this->error['username'])) {
			$data['error_username'] = $this->error['username'];
		} else {
			$data['error_username'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		$data['action'] = $this->url->link('install/step_3');

		if (isset($this->request->post['db_driver'])) {
			$data['db_driver'] = $this->request->post['db_driver'];
		} else {
			$data['db_driver'] = '';
		}

		if (isset($this->request->post['db_hostname'])) {
			$data['db_hostname'] = $this->request->post['db_hostname'];
		} else {
			$data['db_hostname'] = 'localhost';
		}

		if (isset($this->request->post['db_username'])) {
			$data['db_username'] = $this->request->post['db_username'];
		} else {
			$data['db_username'] = 'root';
		}

		if (isset($this->request->post['db_password'])) {
			$data['db_password'] = $this->request->post['db_password'];
		} else {
			$data['db_password'] = '';
		}

		if (isset($this->request->post['db_database'])) {
			$data['db_database'] = $this->request->post['db_database'];
		} else {
			$data['db_database'] = '';
		}
		
		if (isset($this->request->post['db_port'])) {
			$data['db_port'] = $this->request->post['db_port'];
		} else {
			$data['db_port'] = 3306;
		}
		

		$characters_1 = 'abcdefghijklmnopqrstuvwxyz';
		$characters = 'abcdefghijklmnopqrstuvwxyz123456789';
		$string = '';
		
		$max = strlen($characters) - 1;
		$max_1 = strlen($characters_1) - 1;

		$string = $characters_1[mt_rand(0, $max_1)];

		for ($i = 0; $i < 2; $i++) {
			for ($j = 0; $j < 8; $j++) {
				$string .= $characters[mt_rand(0, $max)];
			}
			$string .= "_";	
		}

		if (isset($this->request->post['db_prefix'])) {
			$data['db_prefix'] = $this->request->post['db_prefix'];
		} else {
			$data['db_prefix'] = $string;
		}

		if (isset($this->request->post['username'])) {
			$data['username'] = $this->request->post['username'];
		} else {
			$data['username'] = 'firstcom';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = "password1236";
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = 'gibsernkoh@firstcom.com.sg';
		}

		$data['mysqli'] = extension_loaded('mysqli');
		$data['mysql'] = extension_loaded('mysql');
		$data['pdo'] = extension_loaded('pdo');
		$data['pgsql'] = extension_loaded('pgsql');

		$data['back'] = $this->url->link('install/step_2');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');

		$this->response->setOutput($this->load->view('install/step_3', $data));
	}

	private function validate() {
		if (!$this->request->post['db_hostname']) {
			$this->error['db_hostname'] = $this->language->get('error_db_hostname');
		}

		if (!$this->request->post['db_username']) {
			$this->error['db_username'] = $this->language->get('error_db_username');
		}

		if (!$this->request->post['db_database']) {
			$this->error['db_database'] = $this->language->get('error_db_database');
		}

		if (!$this->request->post['db_port']) {
			$this->error['db_port'] = $this->language->get('error_db_port');
		}		

		if ($this->request->post['db_prefix'] && preg_match('/[^a-z0-9_]/', $this->request->post['db_prefix'])) {
			$this->error['db_prefix'] = $this->language->get('error_db_prefix');
		}

		if ($this->request->post['db_driver'] == 'mysqli') {
			$mysql = @new mysqli($this->request->post['db_hostname'], $this->request->post['db_username'], $this->request->post['db_password'], $this->request->post['db_database'], $this->request->post['db_port']);
			
			$servername = $this->request->post['db_hostname'];
			$username = $this->request->post['db_username'];
			$password = $this->request->post['db_password'];
			
			if ($mysql->connect_error) {
				$conn = @new mysqli($servername, $username, $password);

				$sql = "CREATE DATABASE ".$this->request->post['db_database']." DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci";
				$empty_check = "SHOW TABLES FROM".$this->request->post['db_database'];
				
				if ($conn->query($sql) === TRUE && $conn->query($empty_check) === FALSE){
					$conn->close();
				}else{					
					$this->error['warning'] = "Either wrong login details or the database is in use.";
				}
			} else {
				$mysql->close();
			}
		} elseif ($this->request->post['db_driver'] == 'mpdo') {
			try {
				new \DB\mPDO($this->request->post['db_hostname'], $this->request->post['db_username'], $this->request->post['db_password'], $this->request->post['db_database'], $this->request->post['db_port']);
			} catch(Exception $e) {
				$this->error['warning'] = $e->getMessage();
			}
		}			
		
		if (!$this->request->post['username']) {
			$this->error['username'] = $this->language->get('error_username');
		}

		if (!$this->request->post['password']) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (!is_writable(DIR_OPENCART . 'config.php')) {
			$this->error['warning'] = $this->language->get('error_config') . DIR_OPENCART . 'config.php!';
		}

		if (!is_writable(DIR_OPENCART . 'admin/config.php')) {
			$this->error['warning'] = $this->language->get('error_config') . DIR_OPENCART . 'admin/config.php!';
		}

		return !$this->error;
	}




	private function validate_support() {
		$this->language->load('install/step_2');

		if (phpversion() < '5.4') {
			$this->error['warning'] = $this->language->get('error_version');
		}

		if (!ini_get('file_uploads')) {
			$this->error['warning'] = $this->language->get('error_file_upload');
		}

		if (ini_get('session.auto_start')) {
			$this->error['warning'] = $this->language->get('error_session');
		}

		$db = array(
			'mysql', 
			'mysqli', 
			'pdo', 
			'pgsql'
		);

		if (!array_filter($db, 'extension_loaded')) {
			$this->error['warning'] = $this->language->get('error_db');
		}

		if (!extension_loaded('gd')) {
			$this->error['warning'] = $this->language->get('error_gd');
		}

		if (!extension_loaded('curl')) {
			$this->error['warning'] = $this->language->get('error_curl');
		}

		if (!function_exists('openssl_encrypt')) {
			$this->error['warning'] = $this->language->get('error_openssl');
		}

		if (!extension_loaded('zlib')) {
			$this->error['warning'] = $this->language->get('error_zlib');
		}

		if (!extension_loaded('zip')) {
			$this->error['warning'] = $this->language->get('error_zip');
		}
		
		if (!function_exists('iconv') && !extension_loaded('mbstring')) {
			$this->error['warning'] = $this->language->get('error_mbstring');
		}
		
		if (!file_exists(DIR_OPENCART . 'config.php')) {
			$this->error['warning'] = $this->language->get('error_catalog_exist');
		} elseif (!is_writable(DIR_OPENCART . 'config.php')) {
			$this->error['warning'] = $this->language->get('error_catalog_writable');
		}

		if (!file_exists(DIR_OPENCART . 'admin/config.php')) {
			$this->error['warning'] = $this->language->get('error_admin_exist');
		} elseif (!is_writable(DIR_OPENCART . 'admin/config.php')) {
			$this->error['warning'] = $this->language->get('error_admin_writable');
		}

		if (!is_writable(DIR_OPENCART . 'image')) {
			
		}

		if (!is_writable(DIR_OPENCART . 'image/cache')) {
			$this->error['warning'] = $this->language->get('error_image_cache');
		}

		if (!is_writable(DIR_OPENCART . 'image/catalog')) {
			$this->error['warning'] = $this->language->get('error_image_catalog');
		}
		
		if (!is_writable(DIR_SYSTEM . 'storage/cache')) {
			$this->error['warning'] = $this->language->get('error_cache');
		}

		if (!is_writable(DIR_SYSTEM . 'storage/logs')) {
			$this->error['warning'] = $this->language->get('error_log');
		}

		if (!is_writable(DIR_SYSTEM . 'storage/download')) {
			$this->error['warning'] = $this->language->get('error_download');
		}

		if (!is_writable(DIR_SYSTEM . 'storage/upload')) {
			$this->error['warning'] = $this->language->get('error_upload');
		}

		if (!is_writable(DIR_SYSTEM . 'storage/modification')) {
			$this->error['warning'] = $this->language->get('error_modification');
		}

		return !$this->error;
	}
}
