<?php
class ModelLocalisationLocation extends Model {
	public function getLocation($location_id) {
		$query = $this->db->query("SELECT location_id, name, address, geocode, telephone, fax, image, open, comment, country, retailer FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

		return $query->row;
	}

	public function getLocations() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location");

		return $query->rows;
	}

	public function getSelectedLocations($country_id, $retailer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location WHERE country = '" . (int)$country_id . "'AND retailer='" . (int)$retailer_id . "'");

		return $query->rows;
	}

	public function getStoreLocationsByCountries() {
		$query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "location GROUP BY country");

		return $query->rows;
	}
}