<?php
class ModelCatalogRetailer extends Model {
	public function getRetailer($retailer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "retailer m LEFT JOIN " . DB_PREFIX . "retailer_to_store m2s ON (m.retailer_id = m2s.retailer_id) WHERE m.retailer_id = '" . (int)$retailer_id . "' AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getRetailers($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "retailer m LEFT JOIN " . DB_PREFIX . "retailer_to_store m2s ON (m.retailer_id = m2s.retailer_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$sort_data = array(
				'name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$retailer_data = $this->cache->get('retailer.' . (int)$this->config->get('config_store_id'));

			if (!$retailer_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "retailer m LEFT JOIN " . DB_PREFIX . "retailer_to_store m2s ON (m.retailer_id = m2s.retailer_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY name");

				$retailer_data = $query->rows;

				$this->cache->set('retailer.' . (int)$this->config->get('config_store_id'), $retailer_data);
			}

			return $retailer_data;
		}
	}
}