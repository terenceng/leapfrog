<?php
class ControllerExtensionModuleHomecontent extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'home_content';

    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'home_left_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'home_left_image'),
			'home_left_image_link'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'home_left_image_link'),
			'home_right_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'home_right_image'),
			'home_right_image_link'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'home_right_image_link'),
		);

		return $this->load->view('extension/module/home_content', $data);
	}
}
