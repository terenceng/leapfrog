<?php
class ControllerExtensionModuleSupportDriverscontent extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'support_drivers_content';


		$this->load->model('catalog/manufacturer');
		


    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'downloads'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'downloads'),
		);

		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();
		$data['manufacturers'] = array();

		if(isset($this->request->get['manufacturer_id'])){
			$selected_manufacturer_id= $this->request->get['manufacturer_id'];
		}else {
			$selected_manufacturer_id = $manufacturers[0]['manufacturer_id'];
		}

		$selected_manufacturer = $this->model_catalog_manufacturer->getManufacturer($selected_manufacturer_id);

		//Sidebar
		foreach($manufacturers as $i =>  $manufacturer) {
			$manufacturer_url = 'manufacturer_id='.$manufacturer['manufacturer_id'];
			
			if ($manufacturer['manufacturer_id'] == $selected_manufacturer_id) {
				$active = 1;
			}else {
				$active = 0;
			}

			$url = $this->url->link('information/information', $manufacturer_url . '&information_id=7');
	
			$data['manufacturers'][] = array(
				'name' => $manufacturer['name'],
				'href' => $url,
				'active' => $active,
				'hover_color' => $manufacturer['color'],
				'bg_color'=> $manufacturer['bg_color'],
			);
		}

		//Selected Colors

		$data['bg_color'] = $selected_manufacturer['bg_color'];
		$data['color'] = $selected_manufacturer['color'];
		$data['image']	=	$selected_manufacturer['image2'];
		$data['selected_downloads'] = array();

		$key = array_search($selected_manufacturer_id, array_column($data['downloads'], 'manufacturer'));

		foreach($data['downloads'] as $download) {
			if(!isset($this->request->get['manufacturer_id']) || $download['manufacturer'] == $this->request->get['manufacturer_id']) {
				$data['selected_downloads'][] = array(
					'tab_title'     => $download['tab_title'],
					'tab_image'       => $download['tab_image'],
					'content'     => $download['content'],
				);
			}
		}

		return $this->load->view('extension/module/support_drivers_content', $data);
	}
}
