<?php
class ControllerExtensionModuleAboutcontent extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'about_content';

    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'about_main_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_main_title'),
			'about_sub_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_sub_title'),

			'about_content_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_content_text'),
			'about_content_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_content_image'),

			'bottom_banner_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'bottom_banner_text'),
			'bottom_banner_bg'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'bottom_banner_bg'),
			'partners'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'partners'),
		);

		return $this->load->view('extension/module/about_content', $data);
	}
}
