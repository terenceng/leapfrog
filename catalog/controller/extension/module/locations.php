<?php
class ControllerExtensionModuleLocations extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'locations';

		$this->load->library('modulehelper');
		$Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
		);

		$this->load->model('localisation/country');
		$this->load->model('localisation/location');
		$this->load->model('catalog/retailer');
		$this->load->model('catalog/manufacturer');

		$api_key = $this->config->get('config_google_api');

		$this->document->addScript("https://maps.googleapis.com/maps/api/js?key=$api_key&callback=initMap", "header", true);

		$data['country_names'] = array();

		$all_countries = $this->model_localisation_location->getStoreLocationsByCountries();
		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();

		if(isset($this->request->get['manufacturer_id']) && !empty($this->request->get['manufacturer_id'])) {
			$selected_manufacturer_id= $this->request->get['manufacturer_id'];
		}else {
			$selected_manufacturer_id = $manufacturers[0]['manufacturer_id'];
		}

		$single_manufacturer = $this->model_catalog_manufacturer->getManufacturer($selected_manufacturer_id);

		$data['manufacturer_image'] = HTTP_SERVER .'image/'. $single_manufacturer['image2'];

		if(isset($this->request->get['country_id']) && !empty($this->request->get['country_id'])) {
			$country_id = $this->request->get['country_id'];
		}else {
			$country_id = 188;
		}

		//Find map

		$current_country  = $this->model_localisation_country->getCountry($country_id);
		$this->load->model('tool/image');
		$data['map_marker'] = $this->model_tool_image->resize( $single_manufacturer['image2'] , 107, 35);
		$data['country_lat'] = $current_country['latitude'];
		$data['country_long'] = $current_country['longitude'];

		if(isset($this->request->get['retailer_id'])) {
			$retailer_id = $this->request->get['retailer_id'];
		}else {
			$retailer_id = 2;
		}

		$data['base_url'] = html($this->url->link('information/information', 'information_id=8'));

		$data['base_manufacturer_id'] = $selected_manufacturer_id;

		$data['base_retailer_id'] = $selected_manufacturer_id;
		
		$base_manufacturer_url = '&manufacturer_id='.$selected_manufacturer_id;

		$base_country_url = '&country_id='.$country_id;

		$base_retailer_url = '&retailer_id='.$retailer_id;

		//Sidebar

		$data['hover_color'] = $single_manufacturer['color'];
		foreach($manufacturers as $i =>  $manufacturer) {
			
			$manufacturer_url = '&manufacturer_id='.$manufacturer['manufacturer_id'];

			$url='';
			
			if ($manufacturer['manufacturer_id'] == $selected_manufacturer_id) {
				$active = 1;
			}else {
				$active = 0;
			}

			$url = $this->url->link('information/information', 'information_id=8'. $manufacturer_url . $base_country_url . $base_retailer_url );
	
			$data['manufacturers'][] = array(
				'name' => $manufacturer['name'],
				'href' => $url,
				'active' => $active,
				'hover_color' => $manufacturer['color'],
				'bg_color'=> $manufacturer['bg_color'],
			);
		}

		foreach ($all_countries as $all_country) {
			
			$selected_country_id = $all_country['country'];

			$country_url  = '&country_id='.$selected_country_id;

			$url = $this->url->link('information/information', 'information_id=8'. $base_manufacturer_url . $country_url  . $base_retailer_url );
			$single_country = $this->model_localisation_country->getCountry($all_country['country']);
			
			if($single_country) {
				$data['countries'][] = array(
					'name' => $single_country['name'],
					'value' => $single_country['country_id'],   
					'href' 	=> $url,
					'lat' 	=> $single_country['latitude'],
					'long' 	=> $single_country['longitude'],
					'id' => $single_country['country_id'], 
				);
			}

		};

		$retailers = $this->model_catalog_retailer->getRetailers();

		$data['retailers'] = array();
	
		foreach($retailers as $retailer) {
			$retailer_url = '&retailer_id='. $retailer['retailer_id'];

			$url='';

			$url = $this->url->link('information/information',  '&information_id=8'.$base_manufacturer_url. $base_country_url. $retailer_url);

			if ($retailer['retailer_id'] == $retailer_id) {
				$active = 1;
			}else {
				$active = 0;
			}

			$data['retailers'][]= array(
				'retailer_id' => $retailer['retailer_id'],
				'image' => $retailer['image'],
				'name'	=> $retailer['name'],
				'href' => $url,
				'active'=> $active
			);
		}
		$data['selected_locations'] = array();
		$data['selected_locations'] = $this->model_localisation_location->getSelectedLocations($country_id, $retailer_id);

		return $this->load->view('extension/module/locations', $data);
	}
}
