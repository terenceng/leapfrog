<?php echo $header; ?>

<div class="container">
  <?php echo $content_top; ?>
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
  
    <?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?>">
        <?php if($hide_sidebar_brand) { ?>
          <style>
            body.light-text, body.light-text .product-block .product-name a, body.light-text .input-group-addon  {
              color:#fff!important;
            }

            body.light-text #side-price .price-container>* input {
              color:#fff!important;
            }
            
            #side-manufacturer {
              display:none!important;
            }

            body {
              background:#<?= $bg_color; ?>!important;
            }

            body .container a .more-btn{
              background:#<?= $accent_color; ?>!important;
              border:2px solid #<?= $accent_color; ?>!important;
            }

            body >.container a{
              color:#<?= $accent_color; ?>!important;
            }

            #side-categories .group .item a:hover, body #side-categories .group .item.active a{
              background:#<?= $accent_color; ?>!important;
              color:#fff!important;
            }
            #side-price .ui-slider-range {
              background:#<?= $accent_color; ?>!important;
            }
            #side-price .ui-slider-handle {
              border-color:#<?= $accent_color; ?>!important;
              border
            }
            body .product-block .product-name a:hover {
              color:#4d4d4f;
            }


            body .pagination>.active>a, body .pagination>.active>a:focus, body .pagination>.active>a:hover, body .pagination>.active>span, body .pagination>.active>span:focus, body .pagination>.active>span:hover {
              background:none;
              color:#<?= $accent_color; ?>!important;
              border:1px solid #<?= $accent_color; ?>!important;
            }

            body .pagination>li>a:hover {
              color:#<?= $accent_color; ?>!important;
              border:1px solid #<?= $accent_color; ?>!important;
            }

            .pagination>li:last-child>a, .pagination>l.active>a {
              border:1px solid #<?= $accent_color; ?>!important;
              color:#<?= $accent_color; ?>!important;
            }

          </style>
          
        <?php } ?>
      <div id="product-filter-replace">
        <div id="product-filter-loading-overlay"></div>
        <h2><?php echo $heading_title; ?></h2> 
        
          <?php if ($products) { ?>
          
            <?php include_once('sort_order.tpl'); ?>
              
            <div id="product-filter-detect">
              
              <div class="row">
                <div class="product-view">
                  <?php foreach ($products as $product) { ?>
                    <?php echo $product; ?>
                  <?php } ?>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
              </div>

            </div> <!-- #product-filter-detect -->

          <?php } ?>

          <?php if (!$products) { ?>
          
            <p><?php echo $text_empty; ?></p>
            <div class="buttons hidden">
              <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>

          <?php } ?>

      </div> <!-- #product-filter-replace -->
    </div> <!-- #content -->

    <?php echo $column_right; ?></div>
    <?php echo $content_bottom; ?>
</div>
<script>

  $(document).ready(function(){
    adjustTextColor();
  });
  var element, bgColor, brightness, r, g, b, hsp;

  function adjustTextColor() {

    element = document.getElementsByTagName('body')[0];

    // Get the element's background color
    bgColor = window.getComputedStyle(element, null).getPropertyValue('background-color');

    // Call lightOrDark function to get the brightness (light or dark)
    brightness = lightOrDark(bgColor);

    // If the background color is dark, add the light-text class to it
    if(brightness == 'dark') {
    element.classList.add('light-text');
    }
    else {
    element.classList.add('dark-text');
    }
  }

  function lightOrDark(color) {

    // Check the format of the color, HEX or RGB?
    if (color.match(/^rgb/)) {

    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);

    r = color[1];
    g = color[2];
    b = color[3];
    } 
    else {

    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +("0x" + color.slice(1).replace( 
      color.length < 5 && /./g, '$&$&'
    )
            );

    r = color >> 16;
    g = color >> 8 & 255;
    b = color & 255;
    }

    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(
    0.299 * (r * r) +
    0.587 * (g * g) +
    0.114 * (b * b)
    );

    // Using the HSP value, determine whether the color is light or dark
    if (hsp>127.5) {
      return 'light';
    } 
    else {
      return 'dark';
    }
  }
</script>
<?php echo $footer; ?>
