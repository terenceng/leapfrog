<div id="footer-area">
	
<?php if($mailchimp){ ?>
	<div class="newsletter-section text-center">
		<?= $mailchimp; ?>
	</div>
<?php } ?>

<footer>
	<div class="footer-top">
		<div class="container">
			<div id="scrollTop">
				<img class="img-responsive" src="image/catalog/icon_footer-arrow-top.png"/>
				<span>Back To Top</span>
			</div>
			<div class="footer-upper-contet">
				<!-- <div class="footer-contact-info">
					<h5><?= $store; ?></h5>
					<p class="m0">
					<?= $text_address; ?>: <?= $address; ?><br/>
					<?= $text_telephone; ?>: <a href="tel:<?= $telephone; ?>" ><?= $telephone; ?></a><br/>
					<?php if($fax){ ?>
						<?= $text_fax; ?>: <a href="fax:<?= $fax; ?>" ><?= $fax; ?></a><br/>
					<?php } ?>
					<?= $text_email; ?>: <a href="mailto:<?= $email; ?>" ><?= $email; ?></a><br/>
					</p>
				</div> -->

				<?php if ($menu) { ?>
					<?php foreach($menu as $links){ ?>
					<div class="footer-contact-links">
						<h5 class="hidden">
							<?php if($links['href'] != '#'){ ?>
							<?= $links['name']; ?>
							<?php }else{ ?>
							<a href="<?= $links['href']; ?>" 
								<?php if($links['new_tab']){ ?>
									target="_blank"
								<?php } ?>
								>
								<?= $links['name']; ?></a>
							<?php } ?>
						</h5>
						<?php if($links['child']){ ?>
						<ul class="list-unstyled footer-menu">
							<?php foreach ($links['child'] as $each) { ?>
							<li><a href="<?= $each['href']; ?>"
								<?php if($each['new_tab']){ ?>
									target="_blank"
								<?php } ?>
								
								>
									<?= $each['name']; ?></a></li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
					<?php } ?>
				<?php } ?>

				<?php if($social_icons){ ?>
					<div class="footer-social-icons">
						<?php foreach($social_icons as $icon){ ?>
						<a href="<?= $icon['link']; ?>" title="<?= $icon['title']; ?>" alt="
									<?= $icon['title']; ?>" target="_blank">
							<img src="<?= $icon['icon']; ?>" title="<?= $icon['title']; ?>" class="img-responsive" alt="<?= $icon['title']; ?>" />
						</a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 powered">
					<?= $powered; ?>
				</div>
				<div class="col-xs-12 col-sm-6 text-sm-right fcs">
					<?= $text_fcs; ?>
				</div>
			</div>
		</div>
	</div>
</footer>

</div>

<script>AOS.init({
	once: true
});

$('#scrollTop').click(function () {
	$('html, body').animate({
		scrollTop: 0
	}, 600);
})
</script>
</body></html>