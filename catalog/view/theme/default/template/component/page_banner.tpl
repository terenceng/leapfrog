<div class='page-banner relative'>
    <div class="banner-background" style="background:center/cover no-repeat url(<?= $banner_image; ?>)"></div>
    <!-- <img src="<?= $banner_image; ?>" alt="<?= $page_name; ?>" class="img-responsive hidden-xs" />
    <img src="<?= $mobile_banner_image; ?>" alt="<?= $page_name; ?>" class="img-responsive visible-xs" /> -->

    <div class="page-banner-title absolute position-bottom-center container">
        <?= $title; ?>
    </div>
</div>