
<div class="profile-row wrapper m-b-xl row">
	<div class="col-sm-6">
		<div class="title">
			<h3 class="main-title">
				<?= $about_main_title;?>
			</h3>
			<br>
			<h3 class="sub-title">
				<?= $about_sub_title;?>
			</h3>
		</div>
		<div class="about-content_text">
			<?= html($about_content_text); ?>
		</div>
	</div>

	<div class="col-sm-6 image-col">
		<img class="img-responsive" src="image/<?= $about_content_image; ?>"/>
	</div>
</div>
<div class="cta-banner section max-offset text-center" style="background: center / cover no-repeat url(image/<?= $bottom_banner_bg; ?>) ">
	<div class="container">
			<h3><?= html($bottom_banner_text); ?></h3>
	</div>
</div>
<div class="partner-row wrapper row text-center">
	<div class="partner-title m-b-xl">
		<h3>Brand Partners</h3>
	</div>
	<div class="logo-wrapper">
		<?php foreach($partners as $partner) {?>
			<div class="logo">
				<img class="img-responsive" src ="image/<?=$partner['logo'];?>"/>
			</div>
		<?php } ?>
	</div>
</div>
<script>
	$( document ).ready(function() {
		$(".logo-wrapper").slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1199,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              }
            }
          ],
          prevArrow: "<div class='pointer slick-nav left prev absolute'><i class='fa fa-angle-left fa-5x pmainimg-left'></i></div>",
          nextArrow: "<div class='pointer slick-nav right next absolute'><i class='fa fa-angle-right fa-5x pmainimg-right'></i></div>",
        });
	});
</script>
