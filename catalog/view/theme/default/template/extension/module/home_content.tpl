<div class="row fullwidth">
	<div class="col-sm-6 no-padding">
		<a class="img-zoom" href="<?= $home_left_image_link; ?>">
			<img class="fullwidth-img img-responsive" src="image/<?= $home_left_image; ?>"/>
		</a>
	</div>
	<div class="col-sm-6 no-padding">
		<a class="img-zoom" href="<?= $home_right_image_link; ?>">
			<img class="fullwidth-img img-responsive" src="image/<?= $home_right_image; ?>"/>
		</a>
	</div>
</div>
