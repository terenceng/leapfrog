	
<style>
	#map {
		height: 425px;
	}
	<?php foreach($manufacturers as $i => $manufacturer) { ?>
		.manufacturer-sidebar #manufacturer-<?= $i; ?> a:hover {
			background:#<?= $manufacturer['hover_color'];?>
		}
	<?php } ?>

	body .panel-title>a, .manufacturer a.active  {
		background:#<?= $hover_color; ?>; 
		color:#fff;
	}
</style>

<div class="location-row wrapper row text-center">

	<div class="left manufacturer-sidebar">
		<?php foreach($manufacturers as $i => $manufacturer) { ?>
			<div id="manufacturer-<?= $i; ?>" class="manufacturer">
			<a class="<?php if($manufacturer['active']) echo 'active' ?>" href="<?= $manufacturer['href']?>" style=""><?= $manufacturer['name'];?></a>
			</div>
		<?php } ?>
	</div>

	<div class="right">
			<!-- Map -->
		
		<div class="map-wrapper m-b-lg text-left">
			<div id="map" class="map" data-country-lat="<?=$country_lat;?>" data-country-long="<?=$country_long;?>">

			</div>
		</div>


		<div class="country-wrapper m-b-lg text-left">
			<select id="country" class="select-option"> 
					<option value=""disabled> Select</option>
					<?php foreach($countries as $country) {?>	
						<option value="<?=$country['id']?>"><?=$country['name']?></option>
					<?php } ?>
			</select>
		</div>

		<div class="retailer-wrapper m-b-lg flex">
			<?php foreach($retailers as $retailer) { ?>
				<div class="retailer-block">
					<a class="<?php if($retailer['active']) echo 'active' ?>"href="<?=$retailer['href']?>"><img src="image/<?= $retailer['image']; ?>" class="img-responsive"/></a>	
				</div>
			<?php } ?>
		</div>

		<div class="location-wrapper m-b-lg ">
			<?php foreach($selected_locations as $locations) { ?>
				<div class="location-block col-sm-4" data-lat="<?= $locations['latitude']; ?>" data-long="<?= $locations['longitude']; ?>">
					<strong><div class="title"><?= $locations['name']; ?></div></strong><br>
					<div><?= $locations['address']; ?></div>
					<br>
					<div>Tel:<?= $locations['telephone']; ?></div>
				</div>
			<?php } ?>
		</div>		
	</div>
</div>

<script>
	$( document ).ready(function() {
		$(".retailer-wrapper").slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1199,
              settings: {
                slidesToShow: 6,
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
              }
            }
          ],
          prevArrow: "<div class='pointer slick-nav left prev absolute'><i class='fa fa-angle-left fa-5x pmainimg-left'></i></div>",
          nextArrow: "<div class='pointer slick-nav right next absolute'><i class='fa fa-angle-right fa-5x pmainimg-right'></i></div>",
        });
	});
</script>

<script>

	var url = '<?=$base_url;?>';
	var country_url = '';
	var retailer_url = '&retailer_id=<?=$base_retailer_id;?>';
	var manufacturer_url = '&manufacturer_id=<?=$base_manufacturer_id;?>';
	$('.select-option').change(function(){

	country_url ='&country_id='+encodeURIComponent($(this).val());



	var finalUrl = url + retailer_url + manufacturer_url + country_url;

	window.location.href=finalUrl;

	});

</script>

<script>
$(document).ready(function($) {
	prepareMap();

	function prepareMap() {
		var latlng;
		var latlngs = new Array();
		var latlngsContents = new Array();
		var countrylat;
		var countrylong;
		var lat;
		var long;
		var divId;

		//country center
		if($('#map').attr('data-country-lat').length && $('#map').attr('data-country-long').length )
		{

			countrylat = $('#map').attr('data-country-lat');
			countrylong = $('#map').attr('data-country-long');
			divId = $('#map');
			// if(divId.length)
			// 	divId = divId + '_map';
			console.log(parseFloat($('div.map').attr('data-country-lat')));
			latlng = new google.maps.LatLng(parseFloat(countrylat),parseFloat(countrylong));
		
		}
		
		var i = 0;
		$('div.location-block').each(function(){
			if($(this).attr('data-lat').length && $(this).attr('data-long').length)
			{
				var lat = $(this).attr('data-lat');
				var long = $(this).attr('data-long');

				if(lat && long)
				{
					latlngs[i] = new google.maps.LatLng(parseFloat(lat),parseFloat(long));
					latlngsContents[i] = '<div class="infoWindowContainer"><div class="logo"></div>' + $(this).html() + '</div>';
					i++;
				}
				
			}

		});

		if(!latlng)
			latlng = new google.maps.LatLng(1.30655973783126,103.83156716823578);
		if(latlngs.length == 0)
			latlngs[0] = latlng;
		if(latlng && latlngs && divId)
			printMap(latlngs, latlng, divId, latlngsContents);


	}

	function printMap(latLngs, latlng, divid, latlngsContents){
		if(latLngs && latlng && divid)
		{
			var style= [
				{
				"stylers": [
				{ "invert_lightness": false },
				{ "gamma": 0.85 },
				{ "hue": "#00fff7" },
				{ "lightness": -11 },
				{ "weight": 0.85 },
				{ "saturation": -100 }
				]
				},{
				}
				];

			var myOptions = {
				zoom: 10,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true,
				draggable: true,
				styles: style,
				disableDoubleClickZoom: false,
				zoomControl: true,
				scrollwheel: false
			};
			map = new google.maps.Map(document.getElementById('map'), myOptions);
			var image = new google.maps.MarkerImage(
					'<?=$map_marker;?>',
					new google.maps.Size(107,35),
					new google.maps.Point(0,0),
					new google.maps.Point(25,50)
					);
			var markers = new Array(latLngs.length);
			
			for (var i = 0; i < markers.length; i++) {
				var infowindow = null;
				markers[i] = new google.maps.Marker({
					position: latLngs[i],
					title:"Marker "+i,
					icon: image,
					map: map,
					info: latLngs[i]
				});
				/*
				google.maps.event.addListener(markers[i], 'mouseover', function() {
					placeMarker(event.latLng);
				});
				google.maps.event.addListener(markers[i], 'mouseout', function() {
					placeMarker(event.latLng);
				});
				*/
				//markers[i].setMap(map);
				console.log()
				if(latlngsContents[i])
				{
					
					var infowindow = new google.maps.InfoWindow({content: '', pixelOffset: new google.maps.Size(0, 1)});
					/*
					google.maps.event.addListener(markers[i], 'mouseover', function() {
						//infowindow.open(map,markers[i]);
						//infowindow.setContent(latlngsContents[i]);
						infowindow.open(map, this);
					});
					*/

					console.log(infowindow);
					bindInfoWindow(markers[i], map, infowindow, latlngsContents[i]);
				}
				//markers[i].setMap(map);
				
			}
		}
	}

	function bindInfoWindow(marker, map, infowindow, strDescription) {
		google.maps.event.addListener(marker, 'mouseover', function() {
			infowindow.setContent(strDescription);
			infowindow.open(map, marker);
		});
		google.maps.event.addListener(marker, 'mouseout', function() {
			infowindow.close(map, marker);
		});
	}
});
</script>