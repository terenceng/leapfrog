
	<div class="download-row wrapper row text-center">
	<style>

		body.light-text .panel-title>a,  body.light-text .manufacturer a.active{
			color:#<?= $bg_color; ?>; 
		}

		body.light-text .panel-title>a:hover {
			opacity:0.8;
		}

		body.dark-text .download-item-body {
			border:1px solid #ccc;
		}

		body.light-text .download-row table a{
			color:#fff;
		}	

		body.dark-text .download-row table a{
			color:#4d4d4f;
		}
 	 	
		.manufacturer a:hover {
			background:#<?= $color;?> 
		}

		body .panel-title>a, .manufacturer a.active  {
			background:#<?= $color; ?>; 
			color:#fff;
		}

		body {
			background:#<?= $bg_color; ?>; 
		}

		body.light-text .breadcrumb li:last-child a {
			color:#fff;
		}

		.download-row h4, .download-row table td > *:first-child {
			color:#<?= $color;?>;
		}

	
		<?php foreach($manufacturers as $i => $manufacturer) { ?>
			.manufacturer-sidebar #manufacturer-<?= $i; ?> a:hover {
				background:#<?= $manufacturer['hover_color'];?>
			}
		<?php } ?>
	</style>

		<div class="left manufacturer-sidebar">
			<?php foreach($manufacturers as $i => $manufacturer) { ?>
				<div id="manufacturer-<?= $i; ?>" class="manufacturer">
				<a class="<?php if($manufacturer['active']) echo 'active' ?>" href="<?= $manufacturer['href']?>" style=""><?= $manufacturer['name'];?></a>
				</div>
			<?php } ?>
		</div>
		<div class="right">

		<?php if ($selected_downloads) { ?>

			<?php foreach($selected_downloads as $i => $download) {?>
				<div class="panel-group" id="section<?=$i;?>" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#download-item-<?=$i;?>">
									<?php echo $download['tab_title']; ?>
								</a>
							</h4>
						</div>
						<div id="download-item-<?=$i;?>" class="panel-collapse collapse" role="tabpanel">
							<div class="panel-body download-item-body">
									<div class="category-image">
											<img src="image/<?=html($download['tab_image']); ?>"  class="img-responsive"/>
									</div>
								<?=html($download['content']); ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

		<?php } else { echo 'No drivers available'; }  ?>

		</div>
	</div>
	<script>

$(document).ready(function(){
  adjustTextColor();
});
var element, bgColor, brightness, r, g, b, hsp;

function adjustTextColor() {

  element = document.getElementsByTagName('body')[0];

  // Get the element's background color
  bgColor = window.getComputedStyle(element, null).getPropertyValue('background-color');

  // Call lightOrDark function to get the brightness (light or dark)
  brightness = lightOrDark(bgColor);

  // If the background color is dark, add the light-text class to it
  if(brightness == 'dark') {
  element.classList.add('light-text');
  }
  else {
  element.classList.add('dark-text');
  }
}

function lightOrDark(color) {

  // Check the format of the color, HEX or RGB?
  if (color.match(/^rgb/)) {

  // If HEX --> store the red, green, blue values in separate variables
  color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);

  r = color[1];
  g = color[2];
  b = color[3];
  } 
  else {

  // If RGB --> Convert it to HEX: http://gist.github.com/983661
  color = +("0x" + color.slice(1).replace( 
	color.length < 5 && /./g, '$&$&'
  )
		  );

  r = color >> 16;
  g = color >> 8 & 255;
  b = color & 255;
  }

  // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
  hsp = Math.sqrt(
  0.299 * (r * r) +
  0.587 * (g * g) +
  0.114 * (b * b)
  );

  // Using the HSP value, determine whether the color is light or dark
  if (hsp>127.5) {
	return 'light';
  } 
  else {
	return 'dark';
  }
}
</script>
