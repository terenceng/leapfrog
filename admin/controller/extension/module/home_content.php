<?php
class ControllerExtensionModuleHomecontent extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'Homepage Content',
      'modulename' => 'home_content',
      'fields' => array(
        array('type' => 'image', 'label' => 'Second Section Category Left Image', 'name' => 'home_left_image'),
        array('type' => 'text', 'label' => 'Second Section Category Left Image Link', 'name' => 'home_left_image_link'),
        array('type' => 'image', 'label' => 'Second Section Category Right Image', 'name' => 'home_right_image'),
        array('type' => 'text', 'label' => 'Second Section Category Right Image Link', 'name' => 'home_right_image_link'),
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
