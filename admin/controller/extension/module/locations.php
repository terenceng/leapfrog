<?php
class ControllerExtensionModuleLocations extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'Locations',
      'modulename' => 'locations',
      'fields' => array(
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
