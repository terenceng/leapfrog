<?php
class ControllerExtensionModuleAboutcontent extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'About Page Content',
      'modulename' => 'about_content',
      'fields' => array(
        array('type' => 'text', 'label' => 'About Content Main Title', 'name' => 'about_main_title'),
        array('type' => 'text', 'label' => 'About Content Sub Title', 'name' => 'about_sub_title'),
        array('type' => 'textarea', 'label' => 'About Content Text', 'name' => 'about_content_text'),
        array('type' => 'image', 'label' => 'About Content Image', 'name' => 'about_content_image'),

        array('type' => 'textarea', 'label' => 'About Bottom Banner Text', 'name' => 'bottom_banner_text'),
        array('type' => 'image', 'label' => 'About Bottom Banner Background Image', 'name' => 'bottom_banner_bg'),
        
        array('type' => 'repeater', 'label' => 'About', 'name' => 'partners',
          'fields' => array(
            array ('type' => 'image', 'label' => 'Brands Logo', 'name' => 'logo'),
          )
        ),
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
