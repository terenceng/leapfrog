<?php
class ControllerExtensionModuleSupportDriversContent extends Controller {
  private $error = array();

  public function index() {

       $this->load->model('catalog/manufacturer');
       $datas = $this->model_catalog_manufacturer->getManufacturers();
       $choices = array();
       foreach($datas as $data){
           $choices[] = array(
               'value' => str_replace("'","", $data['manufacturer_id']),
               'label' => str_replace("'", "", $data['name'])
           );
       }    

    $array = array(
      'oc' => $this,
      'heading_title' => 'Support Download Content',
      'modulename' => 'support_drivers_content',
      'fields' => array(
        
        array('type' => 'repeater', 'label' => 'Downloads', 'name' => 'downloads',
          'fields' => array(
            array('type' => 'dropdown', 'label' => 'Manufacturers', 'name' => 'manufacturer', 'choices' => $choices),
            array('type' => 'text', 'label' => 'Tab Title', 'name' => 'tab_title'),
            array('type' => 'image', 'label' => 'Tab Image', 'name' => 'tab_image'),
            array ('type' => 'textarea', 'label' => 'Download Content', 'name' => 'content'),
          )
        ),
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
