<?php
// Heading
$_['heading_title']     = 'Retailers';

// Text
$_['text_success']      = 'Success: You have modified retailers!';
$_['text_list']         = 'Retailer List';
$_['text_add']          = 'Add Retailer';
$_['text_edit']         = 'Edit Retailer';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';

// Column
$_['column_name']       = 'Retailer Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Retailer Name';
$_['entry_store']       = 'Stores';
$_['entry_keyword']     = 'SEO URL';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Help
$_['help_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify retailers!';
$_['error_name']        = 'Retailer Name must be between 2 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_product']     = 'Warning: This retailer cannot be deleted as it is currently assigned to %s products!';
