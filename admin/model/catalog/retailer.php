<?php
class ModelCatalogRetailer extends Model {
	public function addRetailer($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "retailer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$retailer_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "retailer SET image = '" . $this->db->escape($data['image']) . "' WHERE retailer_id = '" . (int)$retailer_id . "'");
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'retailer_id=" . (int)$retailer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('retailer');

		return $retailer_id;
	}

	public function editRetailer($retailer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "retailer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE retailer_id = '" . (int)$retailer_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "retailer SET image = '" . $this->db->escape($data['image']) . "' WHERE retailer_id = '" . (int)$retailer_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "retailer_to_store WHERE retailer_id = '" . (int)$retailer_id . "'");

		if (isset($data['retailer_store'])) {
			foreach ($data['retailer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "retailer_to_store SET retailer_id = '" . (int)$retailer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'retailer_id=" . (int)$retailer_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'retailer_id=" . (int)$retailer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('retailer');
	}

	public function deleteRetailer($retailer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "retailer WHERE retailer_id = '" . (int)$retailer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "retailer_to_store WHERE retailer_id = '" . (int)$retailer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'retailer_id=" . (int)$retailer_id . "'");

		$this->cache->delete('retailer');
	}

	public function getRetailer($retailer_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'retailer_id=" . (int)$retailer_id . "') AS keyword FROM " . DB_PREFIX . "retailer WHERE retailer_id = '" . (int)$retailer_id . "'");

		return $query->row;
	}

	public function getRetailers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "retailer";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getRetailerStores($retailer_id) {
		$retailer_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "retailer_to_store WHERE retailer_id = '" . (int)$retailer_id . "'");

		foreach ($query->rows as $result) {
			$retailer_store_data[] = $result['store_id'];
		}

		return $retailer_store_data;
	}

	public function getTotalRetailers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "retailer");

		return $query->row['total'];
	}
}
